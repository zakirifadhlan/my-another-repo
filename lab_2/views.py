from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implements
#Create a content paragraph for your landing page:
landing_page_content = 'I am a passionate traveler who loves aviation, a blogger, a\
                        travel youtuber. For more information, kindly visit my blog at\
                        hellofz98.wordpress.com'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
